import { DigitalCoachPage } from './app.po';

describe('digital-coach App', () => {
  let page: DigitalCoachPage;

  beforeEach(() => {
    page = new DigitalCoachPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
