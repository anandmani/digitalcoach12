import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { AppComponent } from './app.component';
import {routing} from './app.route';
import { TestformHtmlComponent } from './testform-html/testform-html.component';
import { TestformSwfComponent } from './testform-swf/testform-swf.component';


@NgModule({
  declarations: [
    AppComponent,
    TestformHtmlComponent,
    TestformSwfComponent
  ],
  imports: [
    BrowserModule,
    routing

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
