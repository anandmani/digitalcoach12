import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestformSwfComponent } from './testform-swf.component';

describe('TestformSwfComponent', () => {
  let component: TestformSwfComponent;
  let fixture: ComponentFixture<TestformSwfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestformSwfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestformSwfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
