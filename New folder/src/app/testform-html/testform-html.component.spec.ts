import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestformHtmlComponent } from './testform-html.component';

describe('TestformHtmlComponent', () => {
  let component: TestformHtmlComponent;
  let fixture: ComponentFixture<TestformHtmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestformHtmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestformHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
