import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
@Component({
  selector: 'app-testform-html',
  templateUrl: './testform-html.component.html',
  styleUrls: ['./testform-html.component.scss']
})
export class TestformHtmlComponent implements OnInit {
  prev_btn: boolean = true;
  next_btn: boolean = true;
  msg1: boolean = true;
  msg2: boolean = false;
  msg3: boolean = false;
  msg4: boolean = false;
  msg5: boolean = false;
  msg6: boolean = false;
  msg7: boolean = false;
  verbal: boolean = false;
  wrongans:boolean=false;
  msg8: boolean = false;
  msg9: boolean = false;
  constructor(private router: Router) {
    this.btnenable();

  }

  btnenable() {
    setTimeout(function () {
      let that = this;

      console.log(this.prev_btn)
    }, 6000);
  }
  next_btn1() {
    if (this.msg1 == true) {
      this.msg1 = false;
      this.msg2 = true;
    }

  }

  next_btn2() {
    if (this.msg2 == true) {
      this.msg2 = false;
      this.msg3 = true;
    }

  }
  next_btn3() {
    if (this.msg3 == true) {
      this.msg3 = false;
      this.msg4 = true;
    }
  }
  next_btn4() {
    if (this.msg4 == true) {
      this.msg4 = false;
      this.msg5 = true;
    }
  }
  next_btn5() {
    if (this.msg5 == true) {
      this.msg5 = false;
      this.msg6 = true;
    }
  }
  verbal1() {
    this.verbal = true;
  }
  next_btn6() {
    if (this.msg6 == true) {
      this.msg6 = false;
      this.msg7 = true;
      this.verbal = false;
    }
  }
  next_btn7() {
    if (this.msg7 == true) {
      this.msg7 = false;
      this.msg8 = true;
    }
  }
  next_btn8() {
    if (this.msg8 == true) {
      this.msg8 = false;
      this.msg9 = true;
    }
  }
  wrongchoice(){
    this.wrongans=true;
    setTimeout(()=>{
      this.wrongans=false;
    },2000)
  }



  ngOnInit() {
  }

}
