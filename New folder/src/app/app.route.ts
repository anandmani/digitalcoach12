import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 import { TestformHtmlComponent } from './testform-html/testform-html.component';
 import { TestformSwfComponent } from './testform-swf/testform-swf.component';
export const routes: Routes = [
    { path: '', component: TestformHtmlComponent },
    { path: 'testform-swf', component: TestformSwfComponent },
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);








